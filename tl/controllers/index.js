angular.module('tl', []);
angular.module('tl').controller('tlCtrl', [func]);
function func() {
	var self = this;
	self.tasks = [];
	self.add = function(){
		self.task = {
			done: false,
			name: self.taskName
		};
		console.log(self.task);
		self.tasks.push(self.task);
		self.taskName = '';
		self.type = "button";
	};
	self.del = function(i) {
		self.tasks.splice(i, 1);
	};
	self.done = function(i) {
		var all = document.querySelectorAll(".tasks");
		if (all[i].style.backgroundColor !== "silver") {
			all[i].style.backgroundColor="silver";
		} else {
			all[i].style.backgroundColor="white";
		};
	};
	self.type = "button";
	self.change = function() {
		self.type = "input";
	};
	self.typeView = "block";
	self.switch = function() {
		if (self.typeView != "list") {
			self.typeView = "list";
		} else {
			self.typeView = "block";
		};
	};
};